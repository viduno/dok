# dok

Interactive docker helper

## Installation

Install somewhere in your `$PATH` and make executable

```bash
chmod +x /somewhere/in/path/dok
```

## Usage

dok is an interactive menu tool in the CLI. Start it with the following:

```bash
dok
```

dok will default to filter running docker processes by the current directory name. Follow the menu options to see more or filter results.

## commands

dok functions can also be called from the command line

get state of a container

```bash
dok s
dok state
```

exec into container

```bash
dok e
dok exec
```

tail container logs

```bash
dok l
dok log
```

follow container logs

```bash
dok f
dok follow
```